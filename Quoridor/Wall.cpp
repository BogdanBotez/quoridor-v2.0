#include "Wall.h"

Wall::Wall(const Wall & other)
{
	//Empty
}

Wall::Wall(Wall && other)
{
	*this = std::move(other);
}

Wall & Wall::operator=(Wall && other)
{
	new(&other) Wall;
	return *this;
}
