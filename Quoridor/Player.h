#pragma once

#include "Wall.h"
#include "Pawn.h"
#include "Board.h"

#include <string>
#include <vector>


class Player
{
public:

	Player() = default;
	Player(const std::string& name, const Pawn::Color& color, const uint8_t& numberOfWalls);
	Player(const Player & other);

	//Getters

	std::string GetName() const;
	Pawn::Color GetColor() const;
	uint8_t GetNumberOfWalls() const;
	void DecreaseNumberOfWalls();

	friend std::ostream& operator << (std::ostream& os, const Player& player);

	Pawn m_pawn;

private:
	std::string m_name;
	uint8_t m_numberOfWalls;
};
