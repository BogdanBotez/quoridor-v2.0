#pragma once

#include <iostream>

class Pawn
{
public:
	enum class Color : uint8_t
	{
		None,
		White,
		Black,
		Red,
		Orange
	};

public:
	Pawn();
	Pawn(Color color);
	Pawn(const Pawn &other);
	Pawn(Pawn && other);

	Pawn & operator = (const Pawn & other);
	Pawn & operator = (Pawn && other);

	//Getter,Setter
	Color GetColor() const;
	void SetColor(const Color &color);

	friend std::ostream& operator << (std::ostream& os, const Pawn& pawn);

	~Pawn();
	
	std::pair<uint8_t, uint8_t> location;

private:
	Color m_color;
};

