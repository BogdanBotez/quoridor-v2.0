#include "QuoridorGame.h"
#include <math.h>

uint8_t kInitialRowPlayer1 = 0;
uint8_t kInitialRowPlayer2 = 16;
uint8_t kInitialColumn = 8;
const char kEmptySpace = 254;
const char kWall = 186;

void QuoridorGame::movePawnUp(Board & board, Player & currentPlayer)
{
	board.m_matrix[currentPlayer.m_pawn.location.first - 2][currentPlayer.m_pawn.location.second] =
		std::move(board.m_matrix[currentPlayer.m_pawn.location.first][currentPlayer.m_pawn.location.second]);
	board.m_matrix[currentPlayer.m_pawn.location.first][currentPlayer.m_pawn.location.second] = kEmptySpace;
	currentPlayer.m_pawn.location = std::make_pair(currentPlayer.m_pawn.location.first - 2, currentPlayer.m_pawn.location.second);
}

void QuoridorGame::movePawnDown(Board &board, Player &currentPlayer)
{
	board.m_matrix[currentPlayer.m_pawn.location.first + 2][currentPlayer.m_pawn.location.second] =
		std::move(board.m_matrix[currentPlayer.m_pawn.location.first][currentPlayer.m_pawn.location.second]);
	board.m_matrix[currentPlayer.m_pawn.location.first][currentPlayer.m_pawn.location.second] = kEmptySpace;
	currentPlayer.m_pawn.location = std::make_pair(currentPlayer.m_pawn.location.first + 2, currentPlayer.m_pawn.location.second);
}

void QuoridorGame::movePawnRight(Board & board, Player & currentPlayer)
{
	board.m_matrix[currentPlayer.m_pawn.location.first][currentPlayer.m_pawn.location.second + 2] =
		std::move(board.m_matrix[currentPlayer.m_pawn.location.first][currentPlayer.m_pawn.location.second]);
	board.m_matrix[currentPlayer.m_pawn.location.first][currentPlayer.m_pawn.location.second] = kEmptySpace;
	currentPlayer.m_pawn.location = std::make_pair(currentPlayer.m_pawn.location.first, currentPlayer.m_pawn.location.second + 2);
}

void QuoridorGame::movePawnLeft(Board & board, Player & currentPlayer)
{
	board.m_matrix[currentPlayer.m_pawn.location.first][currentPlayer.m_pawn.location.second - 2] =
		std::move(board.m_matrix[currentPlayer.m_pawn.location.first][currentPlayer.m_pawn.location.second]);
	board.m_matrix[currentPlayer.m_pawn.location.first][currentPlayer.m_pawn.location.second] = kEmptySpace;
	currentPlayer.m_pawn.location = std::make_pair(currentPlayer.m_pawn.location.first, currentPlayer.m_pawn.location.second - 2);
}

void QuoridorGame::jumpOverOpponentUp(Board & board, Player & currentPlayer)
{
	board.m_matrix[currentPlayer.m_pawn.location.first - 4][currentPlayer.m_pawn.location.second] =
		std::move(board.m_matrix[currentPlayer.m_pawn.location.first][currentPlayer.m_pawn.location.second]);
	board.m_matrix[currentPlayer.m_pawn.location.first][currentPlayer.m_pawn.location.second] = kEmptySpace;
	currentPlayer.m_pawn.location = std::make_pair(currentPlayer.m_pawn.location.first - 4, currentPlayer.m_pawn.location.second);
}

void QuoridorGame::jumpOverOpponentDown(Board & board, Player & currentPlayer)
{
	board.m_matrix[currentPlayer.m_pawn.location.first + 4][currentPlayer.m_pawn.location.second] =
		std::move(board.m_matrix[currentPlayer.m_pawn.location.first][currentPlayer.m_pawn.location.second]);
	board.m_matrix[currentPlayer.m_pawn.location.first][currentPlayer.m_pawn.location.second] = kEmptySpace;
	currentPlayer.m_pawn.location = std::make_pair(currentPlayer.m_pawn.location.first + 4, currentPlayer.m_pawn.location.second);
	
}

void QuoridorGame::jumpOverOpponentLeft(Board & board, Player & currentPlayer)
{
	board.m_matrix[currentPlayer.m_pawn.location.first][currentPlayer.m_pawn.location.second - 4] =
		std::move(board.m_matrix[currentPlayer.m_pawn.location.first][currentPlayer.m_pawn.location.second]);
	board.m_matrix[currentPlayer.m_pawn.location.first][currentPlayer.m_pawn.location.second] = kEmptySpace;
	currentPlayer.m_pawn.location = std::make_pair(currentPlayer.m_pawn.location.first, currentPlayer.m_pawn.location.second - 4);
}

void QuoridorGame::jumpOverOpponentRight(Board & board, Player & currentPlayer)
{
	board.m_matrix[currentPlayer.m_pawn.location.first][currentPlayer.m_pawn.location.second + 4] =
		std::move(board.m_matrix[currentPlayer.m_pawn.location.first][currentPlayer.m_pawn.location.second]);
	board.m_matrix[currentPlayer.m_pawn.location.first][currentPlayer.m_pawn.location.second] = kEmptySpace;
	currentPlayer.m_pawn.location = std::make_pair(currentPlayer.m_pawn.location.first, currentPlayer.m_pawn.location.second + 4);
}

bool QuoridorGame::gameStateCheckerPlayer1(Board &board, Player &player) const
{
	if (player.m_pawn.location.first == kInitialRowPlayer2)
		return true;
	return false;
}

bool QuoridorGame::gameStateCheckerPlayer2(Board &board, Player &player) const
{
	if (player.m_pawn.location.first == kInitialRowPlayer1)
		return true;
	return false;
}



static Pawn::Color FromStringToColor(std::string color)
{

	if (color == "Black")
		return Pawn::Color::Black;
	else if (color == "White")
		return Pawn::Color::White;
	else if (color == "Orange")
		return Pawn::Color::Orange;
	else if (color == "Red")
		return Pawn::Color::Red;

	return Pawn::Color::None;
}

void QuoridorGame::Run()
{
	//Initialization
	Board board;
	uint8_t numberOfWalls = 10;
	uint8_t roundNumber = 1;

	std::string playerName;
	std::cout << "First player name: ";
	std::cin >> playerName;
	Player firstPlayer(playerName, Pawn::Color::Black, numberOfWalls);
	std::pair<uint8_t, uint8_t> initialPositionPlayer1 = std::make_pair(kInitialRowPlayer1, kInitialColumn);
	board.m_matrix[initialPositionPlayer1.first][initialPositionPlayer1.second] = "P1";
	firstPlayer.m_pawn.location = std::make_pair(kInitialRowPlayer1, kInitialColumn);

	std::cout << "Second player name: ";
	std::cin >> playerName;
	Player secondPlayer(playerName, Pawn::Color::White, numberOfWalls);
	std::pair<uint8_t, uint8_t> initialPositionPlayer2 = std::make_pair(kInitialRowPlayer2, kInitialColumn);
	board.m_matrix[initialPositionPlayer2.first][initialPositionPlayer2.second] = "P2";
	secondPlayer.m_pawn.location = std::make_pair(kInitialRowPlayer2, kInitialColumn);

	while (true)
	{
		std::cout << "The board looks like this:\n";

		if (roundNumber % 2 != 0)
		{
			system("cls");
			std::cout << board << std::endl;
			std::cout << firstPlayer.GetName() << ", the number of walls you can use: " << static_cast<int>(firstPlayer.GetNumberOfWalls()) << ". You have 2 options: " << std::endl << "1)move the pawn" << std::endl << "2)place a wall" << std::endl;
			int option;
			std::cin >> option;
			switch (option)
			{
			case 1:
			{
				if (board.m_matrix[firstPlayer.m_pawn.location.first - 1][firstPlayer.m_pawn.location.second] == "0" && board.m_matrix[firstPlayer.m_pawn.location.first - 2][firstPlayer.m_pawn.location.second] != "P2")
					std::cout << "You can move the pawn up (1)" << std::endl;
				if (board.m_matrix[firstPlayer.m_pawn.location.first + 1][firstPlayer.m_pawn.location.second] == "0" && board.m_matrix[firstPlayer.m_pawn.location.first + 2][firstPlayer.m_pawn.location.second] != "P2")
					std::cout << "You can move the pawn down (2)" << std::endl;
				if (board.m_matrix[firstPlayer.m_pawn.location.first][firstPlayer.m_pawn.location.second + 1] == "0" && board.m_matrix[firstPlayer.m_pawn.location.first][firstPlayer.m_pawn.location.second + 2] != "P2")
					std::cout << "You can move the pawn right (3)" << std::endl;
				if (board.m_matrix[firstPlayer.m_pawn.location.first][firstPlayer.m_pawn.location.second - 1] == "0" && board.m_matrix[firstPlayer.m_pawn.location.first][firstPlayer.m_pawn.location.second - 2] != "P2")
					std::cout << "You can move the pawn left (4)" << std::endl;
				if (board.m_matrix[firstPlayer.m_pawn.location.first + 2][firstPlayer.m_pawn.location.second] == "P2")
					std::cout << "You can jump over the opponent's pawn down (5)" << std::endl;
				if (board.m_matrix[firstPlayer.m_pawn.location.first - 2][firstPlayer.m_pawn.location.second] == "P2")
					std::cout << "You can jump over the opponent's pawn up (6)" << std::endl;
				if (board.m_matrix[firstPlayer.m_pawn.location.first][firstPlayer.m_pawn.location.second - 2] == "P2")
					std::cout << "You can jump over the opponent's pawn left (7)" << std::endl;
				if (board.m_matrix[firstPlayer.m_pawn.location.first][firstPlayer.m_pawn.location.second + 2] == "P2")
					std::cout << "You can jump over the opponent's pawn right (8)" << std::endl;
				

				int direction;
				std::cin >> direction;
				switch (direction)
				{
				case 1:
				{
					movePawnUp(board, firstPlayer);
					break;
				}
				case 2:
				{
					movePawnDown(board, firstPlayer);
					if (gameStateCheckerPlayer1(board, firstPlayer))
					{
						std::cout << firstPlayer.GetName() << " Won!";
						return;
					}
					break;
				}

				case 3:
				{
					movePawnRight(board, firstPlayer);
					break;
				}

				case 4:
				{
					movePawnLeft(board, firstPlayer);
					break;
				}

				case 5:
				{
					jumpOverOpponentDown(board, firstPlayer);
					break;
				}
				case 6:
				{
					jumpOverOpponentUp(board, firstPlayer);
					break;
				}
				case 7:
				{
					jumpOverOpponentLeft(board, firstPlayer);
					break;
				}
				case 8:
				{
					jumpOverOpponentRight(board, firstPlayer);
					break;
				}
				std::cout << board;
				break;

				}
			}break;
			case 2:
			{
				if (firstPlayer.GetNumberOfWalls() != 0)
				{
					while (true)
					{
						int wallStartingRow, wallEndingRow, wallStartingColumn, wallEndingColumn;
						std::cout << " Enter the row number from where the wall should begin: ";
						std::cin >> wallStartingRow;
						std::cout << "Enter the column number from where the wall should begin: ";
						std::cin >> wallStartingColumn;
						std::cout << "Enter the row number for the wall ending position: ";
						std::cin >> wallEndingRow;
						std::cout << "Enter the row number for the wall ending position: ";
						std::cin >> wallEndingColumn;

						if (board.m_matrix[wallStartingRow][wallStartingColumn] == "0" && board.m_matrix[wallEndingRow][wallEndingColumn] == "0" &&
							abs(wallStartingColumn-wallEndingColumn) <= 1 && abs(wallStartingRow - wallEndingRow) <= 1)
						{
							board.m_matrix[wallStartingRow][wallStartingColumn] = kWall;
							board.m_matrix[wallEndingRow][wallEndingColumn] = kWall;
							firstPlayer.DecreaseNumberOfWalls();
							break;
						}
						else
							std::cout << std::endl << " Error!!! In that position already exist a wall. Please choose other position ";
					}
				}
				else
				{
					std::cout << "You are out of walls" << std::endl;
					roundNumber++;
				}
			}break;
			}
			roundNumber++;
		}
		else
			if (roundNumber % 2 == 0)
			{
				system("cls");
				std::cout << board << std::endl;
				std::cout << secondPlayer.GetName() << " You have 2 options: " << std::endl << "1)move the pawn" << std::endl << "2)place a wall" << std::endl;
				int option;
				std::cin >> option;
				switch (option)
				{
				case 1:
				{
					if (board.m_matrix[secondPlayer.m_pawn.location.first - 1][secondPlayer.m_pawn.location.second] == "0"  && board.m_matrix[secondPlayer.m_pawn.location.first - 2][secondPlayer.m_pawn.location.second] != "P1")
						std::cout << "You can move the pawn up (1)" << std::endl;
					if (board.m_matrix[secondPlayer.m_pawn.location.first + 1][secondPlayer.m_pawn.location.second] == "0" && board.m_matrix[secondPlayer.m_pawn.location.first + 2][secondPlayer.m_pawn.location.second] != "P1")
						std::cout << "You can move the pawn down (2)" << std::endl;
					if (board.m_matrix[secondPlayer.m_pawn.location.first][secondPlayer.m_pawn.location.second + 1] == "0" && board.m_matrix[secondPlayer.m_pawn.location.first][secondPlayer.m_pawn.location.second + 2] != "P1")
						std::cout << "You can move the pawn right (3)" << std::endl;
					if (board.m_matrix[secondPlayer.m_pawn.location.first][secondPlayer.m_pawn.location.second - 1] == "0" && board.m_matrix[secondPlayer.m_pawn.location.first][secondPlayer.m_pawn.location.second - 2] != "P1")
						std::cout << "You can move the pawn left (4)" << std::endl;
					if (board.m_matrix[secondPlayer.m_pawn.location.first + 2][secondPlayer.m_pawn.location.second] == "P1")
						std::cout << "You can jump over the opponent's pawn down (5)" << std::endl;
					if (board.m_matrix[secondPlayer.m_pawn.location.first - 2][secondPlayer.m_pawn.location.second] == "P1")
						std::cout << "You can jump over the opponent's pawn up (6)" << std::endl;
					if (board.m_matrix[secondPlayer.m_pawn.location.first][secondPlayer.m_pawn.location.second - 2] == "P1")
						std::cout << "You can jump over the opponent's pawn left (7)" << std::endl;
					if (board.m_matrix[secondPlayer.m_pawn.location.first][secondPlayer.m_pawn.location.second + 2] == "P1")
						std::cout << "You can jump over the opponent's pawn right (8)" << std::endl;

					int direction;
					std::cin >> direction;
					switch (direction)
					{
					case 1:
					{
						movePawnUp(board, secondPlayer);
						if (gameStateCheckerPlayer2(board, secondPlayer))
						{
							std::cout << secondPlayer.GetName() << " Won!";
							return;
						}
						break;
					}
					case 2:
					{
						movePawnDown(board, secondPlayer);
						break;
					}

					case 3:
					{
						movePawnRight(board, secondPlayer);
						break;
					}

					case 4:
					{
						movePawnLeft(board, secondPlayer);
						break;
					}

					case 5:
					{
						jumpOverOpponentDown(board, secondPlayer);
						break;
					}

					case 6:
					{
						jumpOverOpponentUp(board, secondPlayer);
						break;
					}

					case 7:
					{
						jumpOverOpponentLeft(board, secondPlayer);
						break;
					}

					case 8:
					{
						jumpOverOpponentRight(board, secondPlayer);
						break;
					}

					std::cout << board;
					break;

					}
				}break;
				case 2:
				{
					if (secondPlayer.GetNumberOfWalls() != 0)
					{
						while (true) {
							int wallStartingRow, wallEndingRow, wallStartingColumn, wallEndingColumn;
							std::cout << "Enter the row number from where the wall should begin: ";
							std::cin >> wallStartingRow;
							std::cout << "Enter the column number from where the wall should begin: ";
							std::cin >> wallStartingColumn;
							std::cout << "Enter the row number for the wall ending position: ";
							std::cin >> wallEndingRow;
							std::cout << "Enter the row number for the wall ending position: ";
							std::cin >> wallEndingColumn;

							if (board.m_matrix[wallStartingRow][wallStartingColumn] == "0" && board.m_matrix[wallEndingRow][wallEndingColumn] == "0" 
								&& abs(wallStartingColumn - wallEndingColumn) <= 1 && abs(wallStartingRow - wallEndingRow) <= 1)
							{
								board.m_matrix[wallStartingRow][wallStartingColumn] = kWall;
								board.m_matrix[wallEndingRow][wallEndingColumn] = kWall;
								secondPlayer.DecreaseNumberOfWalls();
								break;
							}
							else
								std::cout << std::endl << " Error!!! In that position already exist a wall. Please choose other position " << std::endl;
						}
					}
					else
					{
						std::cout << "You are out of walls" << std::endl;
						roundNumber++;
					}
				}break;
				}
				roundNumber++;
			}
	}

}

