#include "QuoridorGame.h"
#include "../LoggingExample/Logging/Logging.h"
#include <fstream>

#include <iostream>

int main()
{
	std::ofstream of("syslog.log", std::ios::app);
	Logger logger(of);

	logger.log("Started Application...", Logger::Level::Info);

	QuoridorGame quoridorGame;
	quoridorGame.Run();
	system("pause");
	return 0;
}