#include "Player.h"


Player::Player(const std::string & name, const Pawn::Color& color, const uint8_t & numberOfWalls) :
	m_name(name), m_pawn(color), m_numberOfWalls(numberOfWalls)
{

}

Player::Player(const Player & other) :
	m_name(other.m_name), m_pawn(other.m_pawn), m_numberOfWalls(other.m_numberOfWalls)
{

}

std::string Player::GetName()const
{
	return m_name;
}

Pawn::Color Player::GetColor()const
{
	return m_pawn.GetColor();
}

uint8_t Player::GetNumberOfWalls() const
{
	return m_numberOfWalls;
}

void Player::DecreaseNumberOfWalls()
{
	m_numberOfWalls--;
}


std::ostream & operator<<(std::ostream & os, const Player & player)
{
	return os << player.m_name << " " <<
		player.m_pawn.GetColor() << " " <<
		player.m_numberOfWalls << "\n";
}

