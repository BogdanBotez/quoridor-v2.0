#pragma once

#include "Player.h"

class QuoridorGame
{
public:
	void Run();
	void movePawnUp(Board &board, Player &currentPlayer);
	void movePawnDown(Board &board, Player &currentPlayer);
	void movePawnRight(Board &board, Player &currentPlayer);
	void movePawnLeft(Board &board, Player &currentPlayer);
	void jumpOverOpponentUp(Board &board, Player &currentPlayer);
	void jumpOverOpponentDown(Board &board, Player &currentPlayer);
	void jumpOverOpponentLeft(Board &board, Player &currentPlayer);
	void jumpOverOpponentRight(Board &board, Player &currentPlayer);
	bool gameStateCheckerPlayer1(Board &board, Player &player) const;
	bool gameStateCheckerPlayer2(Board &board, Player &player) const;
};

