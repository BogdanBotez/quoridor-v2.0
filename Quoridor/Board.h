#pragma once
#include "Pawn.h"
#include "Wall.h"
#include <optional>
#include <array>
#include <string>

class Board
{
public:
	static const size_t kWidth = 17;
	static const size_t kHeight = 17;
	static const size_t kSize = kWidth * kHeight;
	static const size_t kPawns = 2;
	static const size_t kWalls = 20;

public:
	using Position = std::pair<uint8_t, uint8_t>;


public:
	Board ();
	Board(const Board& other , Position position);

	friend std::ostream& operator << (std::ostream& os, const Board& board);

	std::string m_matrix[kHeight][kWidth];
};
