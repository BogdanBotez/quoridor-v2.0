#pragma once

#include <iostream>

class Wall{

public:
	Wall() = default;

	Wall(const Wall &other);
	Wall(Wall && other);

	Wall & operator = (Wall && other);
};

