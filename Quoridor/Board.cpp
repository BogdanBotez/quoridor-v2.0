﻿#include "Board.h"
#include <iomanip>


const char kEmptyBoardCell = 254;
const char kEmptyTrenchCell[] = "0";

Board::Board()
{
	for (int line = 0; line < Board::kHeight; line++)
	{
		for (int column = 0; column < Board::kWidth; column++)
		{
			if (line % 2 == 0 && column % 2 == 0)
				m_matrix[line][column] = kEmptyBoardCell;
			else if (line % 2 != 0)
				m_matrix[line][column] = kEmptyTrenchCell;
			else if (line % 2 == 0 && column % 2 != 0)
				m_matrix[line][column] = kEmptyTrenchCell;
		}
	}

}

Board::Board(const Board & other, Position position)
{
	m_matrix[position.first][position.second] = other.m_matrix[position.first][position.second];
}

std::ostream & operator<<(std::ostream & os, const Board & board)
{
	os << std::setw(4);
	for (int column = 0; column < Board::kWidth - 1; ++column)
	{
		os << column << std::setw(4);
	}
	os << Board::kWidth - 1;
	os << std::endl << std::endl;
	for (int line = 0; line < Board::kHeight; line++)
	{
		os << line << std::setw(4);

		if (line <= 9)
			os << " ";

		for (int column = 0; column < Board::kWidth; column++)
		{
			if (column != Board::kWidth - 1)
				os << board.m_matrix[line][column] << std::setw(4);
			else
				os << board.m_matrix[line][column];
		}
		os << std::endl;
	}
	return os;
}

