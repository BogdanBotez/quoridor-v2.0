#include "Pawn.h"

Pawn::Pawn() :
	Pawn(Color::None)
{
	//Empty
}

Pawn::Pawn(Color color) :
	m_color(color)
{
	//static_assert(sizeof(*this) <= 1, "This class should be 1 byte in size");
}

Pawn::Pawn(const Pawn & other)
{
	*this = other;
}

Pawn::Pawn(Pawn && other)
{
	*this = std::move(other);
}

Pawn & Pawn::operator=(const Pawn & other)
{
	m_color = other.m_color;
	return *this;
}

Pawn & Pawn::operator=(Pawn && other)
{
	m_color = other.m_color;
	new(&other) Pawn;
	return *this;
}

Pawn::Color Pawn::GetColor() const
{
	return m_color;
}

void Pawn::SetColor(const Color & color)
{
	m_color = color;
}

Pawn::~Pawn()
{
	m_color = Color::None;
}

std::ostream & operator<<(std::ostream & os, const Pawn & pawn)
{
	return os << static_cast<int>(pawn.m_color) - 1;
}
