#include "stdafx.h"
#include "CppUnitTest.h"
#include "../Quoridor/Pawn.h"
#include <cassert>

using namespace Microsoft::VisualStudio::CppUnitTestFramework;

namespace PawnUnitTests
{
	TEST_CLASS(PawnUnitTests)
	{
	public:

		TEST_METHOD(ConstructorDefault)
		{
			Pawn pawn;
			Assert::IsTrue(pawn.GetColor() == Pawn::Color::None);
		}

		TEST_METHOD(CopyingConstructor)
		{
			Pawn otherPawn(Pawn::Color::Red);
			Pawn pawn(otherPawn);
			if (pawn.GetColor() != otherPawn.GetColor())
				Assert::Fail();
		}
	};
}