#include "stdafx.h"
#include "CppUnitTest.h"
#include "../Quoridor/Board.h"
#include <cassert>

using namespace Microsoft::VisualStudio::CppUnitTestFramework;

const char kEmptyCell = 254;
namespace BoardUnitTests
{
	TEST_CLASS(BoardUnitTests)
	{
	public:

		TEST_METHOD(ConstructorDefault)
		{
			Board board;
			for (int column = 0; column < Board::kWidth; column++)
			{
				if (column % 2 != 0 && board.m_matrix[0][column] != "0")
					Assert::Fail();
			}
		}
	};
}