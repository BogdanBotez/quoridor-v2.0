#include "stdafx.h"
#include "CppUnitTest.h"
#include "../Quoridor/Player.h"
#include <cassert>

using namespace Microsoft::VisualStudio::CppUnitTestFramework;

namespace PlayerUnitTests
{
	TEST_CLASS(PlayerUnitTests)
	{
	public:

		TEST_METHOD(ConstructorDefault1)
		{
			Player player("BOB", Pawn::Color::Red, 10);
			if (player.GetColor() != Pawn::Color::Red)
				Assert::Fail();
		}

		TEST_METHOD(ConstructorDefault2)
		{
			Player player("BOB", Pawn::Color::Red, 10);
			Assert::IsTrue(player.GetName() == "BOB");
		}

		TEST_METHOD(ConstructorDefault3)
		{
			Player player("BOB", Pawn::Color::Red, 10);
			Assert::IsTrue(player.GetNumberOfWalls() == 10);
		}
	};
}